package ru.itprogram.configs;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.context.annotation.Configuration;
import ru.itprogram.model.dto.CardDto;
import ru.itprogram.model.dto.CustomerDto;
import ru.itprogram.model.dto.PhoneDto;
import ru.itprogram.model.entity.Card;
import ru.itprogram.model.entity.Customer;
import ru.itprogram.model.entity.Phone;

@Configuration
public class MapperConfiguration extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(Card.class, CardDto.class).byDefault().register();

        factory.classMap(Phone.class, PhoneDto.class).byDefault().register();

        factory.classMap(Customer.class, CustomerDto.class).byDefault().register();
    }

}
