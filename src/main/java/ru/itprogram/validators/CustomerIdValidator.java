package ru.itprogram.validators;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.itprogram.model.entity.Customer;

@Component
public class CustomerIdValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        Customer customer = (Customer) target;
        if (customer.getId() < 0) {
            errors.rejectValue("id", Strings.EMPTY, "В БД нельзя сохранить такой id!");
        }
    }
}
