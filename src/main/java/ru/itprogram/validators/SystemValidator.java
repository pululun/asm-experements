package ru.itprogram.validators;

import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;
import ru.itprogram.model.entity.Customer;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class SystemValidator implements Validator {

    private final javax.validation.Validator validator;
    private final CustomerIdValidator customerIdValidator;
    private SpringValidatorAdapter springValidatorAdapter;

    @PostConstruct
    private void init() {
        springValidatorAdapter = new SpringValidatorAdapter(validator);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Customer.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        springValidatorAdapter.validate(o, errors);

        checkLastNameIsNotNull((Customer) o, errors);

        customerIdValidator.validate(o, errors);
    }

    private void checkLastNameIsNotNull(Customer customer, Errors errors) {
        if (customer.getLastName() == null) {
            errors.rejectValue("lastName", Strings.EMPTY, "Фамилия не может быть пустой!");
        }
    }
}
