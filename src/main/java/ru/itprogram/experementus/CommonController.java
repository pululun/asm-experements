package ru.itprogram.experementus;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.*;
import ru.itprogram.model.dto.CardDto;
import ru.itprogram.model.dto.CustomerDto;
import ru.itprogram.model.dto.PhoneDto;
import ru.itprogram.model.entity.Card;
import ru.itprogram.model.entity.Customer;
import ru.itprogram.repository.CustomerRepository;
import ru.itprogram.service.CustomerService;
import ru.itprogram.validators.SystemValidator;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class CommonController {
    private final SystemValidator systemValidator;
//    private final CustomerRepository repository;
    private final CustomerService customerService;

    @RequestMapping(value = "/q", method = RequestMethod.GET)
//    public ResponseEntity<List<Customer>> get(@RequestParam String name) {
    public ResponseEntity<List<CustomerDto>> get() {
//        List<Customer> customerList = repository.findByLastName(name);
//        if (customerList.size() == 0) {
//            add();
//        }
//        customerList.forEach(customer -> System.out.println("НАЙДЕН!!! [ " + customer + "]"));
        return ResponseEntity.ok(customerService.getAllCustomer());
    }

//    private void add() {
//        // save a couple of customers
//        repository.save(new Customer(0,"Jack", "Bauer", null));
//        repository.save(new Customer(0,"Chloe", "O'Brian", null));
//        repository.save(new Customer(0,"Kim", "Bauer", null));
//        repository.save(new Customer(0,"David", "Palmer", null));
//        repository.save(new Customer(0,"Michelle", "Dessler", null));
//    }

    @RequestMapping(value = "/c", method = RequestMethod.POST)
//    public ResponseEntity<HttpStatus> create(@RequestBody Customer customer) {
    public ResponseEntity<HttpStatus> create() {
//        final DataBinder dataBinder = new DataBinder(customer);
//        dataBinder.addValidators(systemValidator);
//        dataBinder.validate();
//        if (dataBinder.getBindingResult().hasErrors()) {
//            dataBinder.getBindingResult().getAllErrors().forEach(error -> System.out.println(error.toString()));
//        } else {
//            repository.save(customer);
//        }
        add();
        return ResponseEntity.ok().build();
    }

    // save a couple of customers
    private void add() {
        List<PhoneDto> phoneDtoList = new ArrayList<>();
        phoneDtoList.add(new PhoneDto(0, "892775548", 1l));
        phoneDtoList.add(new PhoneDto(0, "892775549", 1l));
        phoneDtoList.add(new PhoneDto(0, "892775550", 1l));
        customerService.createCustomer(
                new CustomerDto(0, "Jack", "Bauer", new CardDto(1, "55555", LocalDate.now()), phoneDtoList));
    }
}
