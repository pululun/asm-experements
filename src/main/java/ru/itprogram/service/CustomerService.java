package ru.itprogram.service;

import ru.itprogram.model.dto.CustomerDto;

import java.util.List;

public interface CustomerService {
    List<CustomerDto> getAllCustomer();
    void createCustomer(CustomerDto customerDto);
}
