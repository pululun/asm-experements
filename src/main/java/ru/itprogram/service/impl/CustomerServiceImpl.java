package ru.itprogram.service.impl;

import lombok.RequiredArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Component;
import ru.itprogram.model.dto.CardDto;
import ru.itprogram.model.dto.CustomerDto;
import ru.itprogram.model.dto.PhoneDto;
import ru.itprogram.model.entity.Card;
import ru.itprogram.model.entity.Customer;
import ru.itprogram.model.entity.Phone;
import ru.itprogram.repository.CardRepository;
import ru.itprogram.repository.CustomerRepository;
import ru.itprogram.repository.PhoneRepository;
import ru.itprogram.service.CustomerService;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final CardRepository cardRepository;
    private final PhoneRepository phoneRepository;
    private final MapperFacade mapperFacade;

    @Override
    public List<CustomerDto> getAllCustomer() {
        return mapperFacade.mapAsList(customerRepository.findAll(), CustomerDto.class);
    }

    @Override
    public void createCustomer(CustomerDto customerDto) {
        createCard(customerDto.getCardDto());
        createPhone(customerDto.getPhoneDtoList());
        customerRepository.save(mapperFacade.map(customerDto, Customer.class));
    }

    public void createPhone(List<PhoneDto> phoneDtoList) {
        phoneDtoList.forEach(phoneDto -> phoneRepository.save(mapperFacade.map(phoneDto, Phone.class)));
    }

    public List<PhoneDto> getPhones(Long id) {
        return mapperFacade.mapAsList(phoneRepository.findByCustomerId(id), PhoneDto.class);
    }

    public void createCard(CardDto cardDto) {
        cardRepository.save(mapperFacade.map(cardDto, Card.class));
    }

    public List<CardDto> getCard() {
        return mapperFacade.mapAsList(cardRepository.findAll(), CardDto.class);
    }
}
