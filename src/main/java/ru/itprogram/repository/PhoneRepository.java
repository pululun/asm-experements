package ru.itprogram.repository;

import org.springframework.data.repository.CrudRepository;
import ru.itprogram.model.entity.Phone;

import java.util.List;

public interface PhoneRepository extends CrudRepository<Phone, Long> {

    List<Phone> findByCustomerId(Long id);
}
