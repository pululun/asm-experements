package ru.itprogram.repository;

import org.springframework.data.repository.CrudRepository;
import ru.itprogram.model.entity.Card;

public interface CardRepository extends CrudRepository<Card, Long> {
}
