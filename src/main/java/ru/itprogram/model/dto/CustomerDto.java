package ru.itprogram.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {

    private long id;
    private String firstName;
    private String lastName;
    private CardDto cardDto;
    private List<PhoneDto> phoneDtoList;
}
